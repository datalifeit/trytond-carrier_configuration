# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from .configuration import Configuration


def register():
    Pool.register(
        Configuration,
        module='carrier_configuration', type_='model')
